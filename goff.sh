set -e
clear

rm -rf ./out/gitlab.com/silenteer-oss/goff/goff.pb.go
mkdir -p ./out
rm -rf ./out/* || true

go install github.com/golang/protobuf/protoc-gen-go

protoc --proto_path=. --go_out=./out ./goff.proto

cp -rf ./out/gitlab.com/silenteer-oss/goff/goff.pb.go ./
