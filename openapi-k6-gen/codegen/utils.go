package codegen

import (
	"fmt"
	"regexp"
	"sort"
	"strings"
	"unicode"

	"github.com/iancoleman/strcase"

	"github.com/getkin/kin-openapi/openapi3"
	"github.com/pkg/errors"
)

var pathParamRE *regexp.Regexp

func init() {
	pathParamRE = regexp.MustCompile("{[.;?]?([^{}*]+)\\*?}")
}

func ToCamelCase(str string) string {
	return strcase.ToCamel(str)
}

// This function returns the keys of the given SchemaRef dictionary in sorted
// order, since Golang scrambles dictionary keys
func SortedSchemaKeys(dict map[string]*openapi3.SchemaRef) []string {
	keys := make([]string, len(dict))
	i := 0
	for key := range dict {
		keys[i] = key
		i++
	}
	sort.Strings(keys)
	return keys
}

// This function checks whether the specified string is present in an array
// of strings
func StringInArray(str string, array []string) bool {
	for _, elt := range array {
		if elt == str {
			return true
		}
	}
	return false
}

// This function takes a $ref value and converts it to a Go typename.
// #/components/schemas/Foo -> Foo
// #/components/parameters/Bar -> Bar
// #/components/responses/Baz -> Baz
// Remote components (document.json#/Foo) are not yet supported
// URL components (http://deepmap.com/schemas/document.json#Foo) are not yet
// supported
// We only support flat components for now, so no components in a schema under
// components.
func RefPathToGoType(refPath string) (string, error) {
	pathParts := strings.Split(refPath, "/")
	if pathParts[0] != "#" {
		return "", errors.New("Only local document components are supported")
	}
	if len(pathParts) != 4 {
		return "", errors.New("Parameter nesting is deeper than supported")
	}
	return SchemaNameToTypeName(pathParts[3]), nil
}

// Converts a Schema name to a valid Go type name. It converts to camel case, and makes sure the name is
// valid in Go
func SchemaNameToTypeName(name string) string {
	name = ToCamelCase(name)
	// Prepend "N" to schemas starting with a number
	if name != "" && unicode.IsDigit([]rune(name)[0]) {
		name = "N" + name
	}
	return name
}

// According to the spec, additionalProperties may be true, false, or a
// schema. If not present, true is implied. If it's a schema, true is implied.
// If it's false, no additional properties are allowed. We're going to act a little
// differently, in that if you want additionalProperties code to be generated,
// you must specify an additionalProperties type
// If additionalProperties it true/false, this field will be non-nil.
func SchemaHasAdditionalProperties(schema *openapi3.Schema) bool {
	if schema.AdditionalPropertiesAllowed != nil {
		return *schema.AdditionalPropertiesAllowed
	}
	if schema.AdditionalProperties != nil {
		return true
	}
	return false
}

// This converts a path, like Object/field1/nestedField into a go
// type name.
func PathToTypeName(path []string) string {
	for i, p := range path {
		path[i] = ToCamelCase(p)
	}
	return strings.Join(path, "_")
}

// StringToGoComment renders a possible multi-line string as a valid Go-Comment.
// Each line is prefixed as a comment.
func StringToGoComment(in string) string {
	// Normalize newlines from Windows/Mac to Linux
	in = strings.Replace(in, "\r\n", "\n", -1)
	in = strings.Replace(in, "\r", "\n", -1)

	// Add comment to each line
	var lines []string
	for _, line := range strings.Split(in, "\n") {
		lines = append(lines, fmt.Sprintf("// %s", line))
	}
	in = strings.Join(lines, "\n")

	// in case we have a multiline string which ends with \n, we would generate
	// empty-line-comments, like `// `. Therefore remove this line comment.
	in = strings.TrimSuffix(in, "\n// ")
	return in
}
