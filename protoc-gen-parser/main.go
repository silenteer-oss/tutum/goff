package main

import (
	"io/ioutil"
	"log"
	"os"

	"github.com/golang/protobuf/protoc-gen-go/descriptor"
	plugin "github.com/golang/protobuf/protoc-gen-go/plugin"
)

var request plugin.CodeGeneratorRequest
var currentFile *descriptor.FileDescriptorProto

func main() {
	log.SetPrefix("protoc-api-parser: ")

	data, err := ioutil.ReadAll(os.Stdin)
	if err != nil {
		log.Fatalf("error: reading input: %v", err)
	}

	_ = ioutil.WriteFile("./out/todo.bff.data", data, 0777)

}
