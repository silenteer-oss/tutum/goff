
go install ./protoc-gen-dbconfig

# co = config output file
# cp = config package name
# fo fixture output file
# fp config package name

protoc-gen-dbconfig \
 -co=$(pwd)/out/gitlab.com/silenteer-oss/goff/todo/repo/dbconfig.gen.go \
 -cp=client \
 -fo=$(pwd)/out/gitlab.com/silenteer-oss/goff/todo/repo/fixture.gen.go \
 -fp=fixture $(pwd)/proto/repo
