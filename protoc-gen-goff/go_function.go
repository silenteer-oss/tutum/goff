package main

import (
	"fmt"
	"log"
	"path"
	"strings"

	"github.com/fatih/structtag"

	"gitlab.com/silenteer-oss/goff"
	"google.golang.org/protobuf/types/descriptorpb"

	"github.com/golang/protobuf/proto"
	"github.com/golang/protobuf/protoc-gen-go/descriptor"
	"gitlab.com/silenteer-oss/goff/protoc-gen-goff/generator"
)

// getGoPackage returns the file's go_package option.
// If it contain a semicolon, only the part before it is returned.
func getGoPackage(fd *descriptor.FileDescriptorProto) string {
	pkg := fd.GetOptions().GetGoPackage()
	if strings.Contains(pkg, ";") {
		parts := strings.Split(pkg, ";")
		if len(parts) > 2 {
			log.Fatalf(
				"protoc-api-nrpc: go_package '%s' contains more than 1 ';'",
				pkg)
		}
		pkg = parts[0]
	}

	return pkg
}

// goPackageOption interprets the file's go_package option.
// If there is no go_package, it returns ("", "", false).
// If there's a simple name, it returns ("", pkg, true).
// If the option implies an import path, it returns (impPath, pkg, true).
func goPackageOption(d *descriptor.FileDescriptorProto) (impPath, pkg string, ok bool) {
	pkg = getGoPackage(d)
	if pkg == "" {
		return
	}
	ok = true
	// The presence of a slash implies there's an import path.
	slash := strings.LastIndex(pkg, "/")
	if slash < 0 {
		return
	}
	impPath, pkg = pkg, pkg[slash+1:]
	// A semicolon-delimited suffix overrides the package name.
	sc := strings.IndexByte(impPath, ';')
	if sc < 0 {
		return
	}
	impPath, pkg = impPath[:sc], impPath[sc+1:]
	return
}

// goFileName returns the output name for the generated Go file.
func goFileName(d *descriptor.FileDescriptorProto) string {
	name := *d.Name
	if ext := path.Ext(name); ext == ".proto" || ext == ".protodevel" {
		name = name[:len(name)-len(ext)]
	}
	name += ".d.go"

	// Does the file have a "go_package" option?
	// If it does, it may override the filename.
	if impPath, _, ok := goPackageOption(d); ok && impPath != "" {
		// Replace the existing dirname with the declared import path.
		_, name = path.Split(name)
		name = path.Join(impPath, name)
		return name
	}

	return name
}

func goType(field *descriptor.FieldDescriptorProto, isPointerStrictMode bool) string {
	repeated := isRepeated(field)
	if field.TypeName != nil {
		var typeName string

		if strings.Compare(".goff.UUID", *field.TypeName) == 0 {
			typeName = "uuid.UUID"
		}

		if strings.Compare(".goff.HttpResponse", *field.TypeName) == 0 {
			if isRepeated(field) {
				return "[]titan.Response"
			}
			// Response should be always be nullable since if there is any returned err, response can be nil.
			return "*titan.Response"
		}

		if strings.Compare(".goff.UserInfo", *field.TypeName) == 0 {
			typeName = "titan.UserInfo"
		}
		if strings.Compare(".goff.HttpHeader", *field.TypeName) == 0 {
			return "map[string][]string"
		}
		if strings.Compare(".goff.Time", *field.TypeName) == 0 {
			typeName = "time.Time"
		}
		if len(typeName) > 0 {
			if repeated {
				// with old code gen's logic, it does not put pointer in slice of types. So there is no point to use strict mode here.
				return fmt.Sprintf("%s%s", "[]", typeName)
			}
			if isNullable(field) || !isPointerStrictMode {
				return fmt.Sprintf("%s%s", "*", typeName)
			}
			return typeName
		}
	}

	typ := ""

	switch *field.Type {
	case descriptor.FieldDescriptorProto_TYPE_DOUBLE:
		typ = "float64"
	case descriptor.FieldDescriptorProto_TYPE_FLOAT:
		typ = "float32"
	case descriptor.FieldDescriptorProto_TYPE_INT64:
		typ = "int64"
	case descriptor.FieldDescriptorProto_TYPE_UINT64:
		typ = "uint64"
	case descriptor.FieldDescriptorProto_TYPE_INT32:
		typ = "int32"
	case descriptor.FieldDescriptorProto_TYPE_UINT32:
		typ = "uint32"
	case descriptor.FieldDescriptorProto_TYPE_FIXED64:
		typ = "uint64"
	case descriptor.FieldDescriptorProto_TYPE_FIXED32:
		typ = "uint32"
	case descriptor.FieldDescriptorProto_TYPE_BOOL:
		typ = "bool"
	case descriptor.FieldDescriptorProto_TYPE_STRING:
		typ = "string"
	case descriptor.FieldDescriptorProto_TYPE_SFIXED32:
		typ = "int32"
	case descriptor.FieldDescriptorProto_TYPE_SFIXED64:
		typ = "int64"
	case descriptor.FieldDescriptorProto_TYPE_SINT32:
		typ = "int32"
	case descriptor.FieldDescriptorProto_TYPE_SINT64:
		typ = "int64"
	case descriptor.FieldDescriptorProto_TYPE_BYTES:
		typ = "[]byte"
	case descriptor.FieldDescriptorProto_TYPE_GROUP, descriptor.FieldDescriptorProto_TYPE_MESSAGE:
		desc := g.ObjectNamed(field.GetTypeName())
		descriptor, ok := desc.(*generator.Descriptor)
		if ok && len(descriptor.Field) == 2 && len(descriptor.TypeName()) == 2 {
			repeated = false
			typ = fmt.Sprintf("map[%s]%s", goType(descriptor.Field[0], isPointerStrictMode), goType(descriptor.Field[1], isPointerStrictMode))
		} else {
			var pointerPrefix string
			if !isPointerStrictMode {
				pointerPrefix = "*"
			}
			typ = fmt.Sprintf("%s%s", pointerPrefix, g.TypeName(desc))
		}
	case descriptor.FieldDescriptorProto_TYPE_ENUM:
		desc := g.ObjectNamed(field.GetTypeName())
		typ = g.TypeName(desc)
	default:
		g.Fail("unknown type for", field.GetName())
	}
	if repeated {
		typ = "[]" + typ
	}
	if isNullable(field) && !strings.HasPrefix(typ, "*") && !(isPointerStrictMode && repeated) {
		typ = "*" + typ
	}

	return typ

}

func goTypeS(typName string, isPointerStrictMode bool) string {
	defaultPointerPrefix := "*"
	if isPointerStrictMode {
		defaultPointerPrefix = ""
	}
	if strings.Compare(".goff.Empty", typName) == 0 {
		return ""
	}
	if strings.Compare(".goff.HttpResponse", typName) == 0 {
		return "*titan.Response"
	}
	if strings.Compare(".goff.UserInfo", typName) == 0 {
		return fmt.Sprintf("%s%s", defaultPointerPrefix, "titan.UserInfo")
	}
	if strings.Compare(".goff.HttpHeader", typName) == 0 {
		return "map[string][]string"
	}

	desc := g.ObjectNamed(typName)
	typ := defaultPointerPrefix + g.TypeName(desc)
	return typ
}

func getGoModelMessage(fd *descriptor.FileDescriptorProto) *descriptorpb.DescriptorProto {
	for _, m := range fd.GetMessageType() {
		if opts := m.GetOptions(); opts != nil {
			if eModel, err := proto.GetExtension(opts, goff.E_Model); err == nil {
				if eModel != nil && *eModel.(*bool) {
					return m
				}
			}
		}
	}

	return nil
}

func getGoModelFieldName(field *descriptor.FieldDescriptorProto, messageName string, modelName string) string {
	prefix := "Field_"
	if messageName != modelName {
		prefix = prefix + messageName + "_"
	}

	return prefix + uppercaseFirst(*field.Name)
}

func getGoModelFieldCustomOption(f *descriptor.FieldDescriptorProto, messageName string, modelName string) string {
	options := getFieldOptionCustom(f)
	bsonName := getBsonName(f, messageName, modelName)

	if !strings.Contains(options, "bson:") {
		result := fmt.Sprintf("bson:\"%s\"", bsonName)
		if options != "" {
			result = result + " " + options
		}
		return result
	}

	tags, err := structtag.Parse(options)
	if err != nil {
		panic(err)
	}

	jsonTag, err := tags.Get("bson")
	if err != nil {
		panic(err)
	}

	if jsonTag.Name == bsonName {
		return options
	} else {
		jsonTag.Options = append([]string{jsonTag.Name}, jsonTag.Options...)
		jsonTag.Name = bsonName
		return tags.String()
	}
}

func getFieldCustomOption(f *descriptor.FieldDescriptorProto) string {
	options := getFieldOptionCustom(f)
	jsonName := lowercaseFirst(*f.Name)

	if !strings.Contains(options, "json:") {
		result := fmt.Sprintf("json:\"%s\"", jsonName)
		if options != "" {
			result = result + " " + options
		}
		return result
	}

	tags, err := structtag.Parse(options)
	if err != nil {
		panic(err)
	}

	jsonTag, err := tags.Get("json")
	if err != nil {
		panic(err)
	}

	if jsonTag.Name == jsonName || jsonTag.Name == "-" {
		return options
	} else {
		jsonTag.Options = append([]string{jsonTag.Name}, jsonTag.Options...)
		jsonTag.Name = jsonName
		return tags.String()
	}
}

func getGoMethodSecured(f *descriptor.MethodDescriptorProto) string {
	secured := getMethodSecured(f)
	if secured == "" {
		return ""
	}

	var rules []string
	var roles []string

	for _, opt := range strings.Split(secured, ",") {
		opt := strings.Trim(opt, " ")
		if strings.EqualFold("IS_AUTHENTICATED", opt) {
			rules = append(rules, "titan.IsAuthenticated()")
		} else if strings.EqualFold("IS_ANONYMOUS", opt) {
			rules = append(rules, "titan.IsAnonymous()")
		} else if strings.EqualFold("DENY_ALL", opt) {
			rules = append(rules, "titan.DenyAll()")
		} else {
			roles = append(roles, "infra."+strings.ToUpper(opt))
		}
	}

	if len(roles) > 0 {
		rules = append(rules, "titan.Secured("+strings.Join(roles, ",")+")")
	}
	return strings.Join(rules, ",")
}

func getGoMessageExtendFromType(m *descriptor.DescriptorProto) string {
	if opts := m.GetOptions(); opts != nil {
		if e, err := proto.GetExtension(opts, goff.E_ExtendFromType); err == nil {
			return *(e.(*string))
		}
	}
	return ""
}

func getGoEnumFromType(m *descriptor.EnumDescriptorProto) string {
	if opts := m.GetOptions(); opts != nil {
		if e, err := proto.GetExtension(opts, goff.E_OfType); err == nil {
			return *(e.(*string))
		}
	}
	return ""
}

func createEventNatsNotifierName(f *descriptor.FileDescriptorProto) string {
	publisherName := createNotifierName(goPackageName(f))
	publisherName = publisherName + "Notifier"
	return publisherName
}

func createEventSocketNotifierName(packageName string) string {
	publisherName := createNotifierName(packageName)
	publisherName = publisherName + "SocketNotifier"
	return publisherName
}

func createNotifierName(packageName string) string {
	publisherName := strings.TrimSpace(packageName)
	if len(publisherName) > 3 {
		publisherName = strings.TrimSuffix(publisherName, "api")
		publisherName = strings.TrimSuffix(publisherName, "Api")
	}
	publisherName = strings.ReplaceAll(publisherName, "_", " ")
	publisherName = strings.ReplaceAll(publisherName, ".", " ")
	publisherName = strings.ReplaceAll(publisherName, "-", " ")
	publisherName = strings.ReplaceAll(publisherName, "/", " ")
	publisherName = strings.Title(publisherName)
	publisherName = strings.ReplaceAll(publisherName, " ", "")

	return publisherName
}

func getEventListenerName(inputType string, isPointerStrictMode bool) string {
	if !strings.Contains(inputType, ".") {
		return "EVENT_" + getEventName(inputType)
	}

	if !strings.HasPrefix(inputType, ".") {
		inputType = "." + inputType
	}

	name := goTypeS(inputType, isPointerStrictMode)
	lstIndex := strings.LastIndex(name, ".")
	if lstIndex > -1 {
		pkg := name[:lstIndex]
		event := name[lstIndex+1:]
		return strings.TrimPrefix(pkg+"."+"EVENT_"+getEventName(event), "*")
	} else {
		return "EVENT_" + strings.TrimPrefix(strings.TrimPrefix(getEventName(name), "*"), "Event")
	}
}
