package main

import (
	"bytes"
	_ "embed"
	"fmt"
	"go/format"
	"io/ioutil"
	"log"
	"os"
	"reflect"
	"strings"
	"text/template"
	"unicode"

	path2 "gitlab.com/silenteer-oss/goff/protoc-gen-goff/pkg/path"

	"github.com/spf13/viper"

	"github.com/iancoleman/strcase"

	"gitlab.com/silenteer-oss/goff"

	"gitlab.com/silenteer-oss/goff/protoc-gen-goff/generator"

	"github.com/golang/protobuf/proto"
	"github.com/golang/protobuf/protoc-gen-go/descriptor"

	plugin "github.com/golang/protobuf/protoc-gen-go/plugin"
)

// Is this field repeated?
func isRepeated(field *descriptor.FieldDescriptorProto) bool {
	return field.Label != nil && *field.Label == descriptor.FieldDescriptorProto_LABEL_REPEATED
}

func getEventName(typName string) string {
	eventName := strings.TrimSpace(typName)
	lastIndex := strings.LastIndex(eventName, ".")
	if lastIndex >= 0 {
		eventName = eventName[lastIndex+1:]
	}
	eventName = strings.TrimPrefix(eventName, "Event")
	eventName = strings.TrimPrefix(eventName, "event")
	eventName = strings.TrimPrefix(eventName, "EVENT")
	return strings.Title(eventName)
}

func getServiceName(queue string, s *descriptor.ServiceDescriptorProto) string {
	// if there is a custom configuration for name
	if opts := s.GetOptions(); opts != nil {
		if e, err := proto.GetExtension(opts, goff.E_Name); err == nil {
			return *(e.(*string))
		}
	}

	// get service name
	name := *s.Name
	suffixes := []string{"Service", "App", "Bff"}

	for _, suf := range suffixes {
		if strings.HasSuffix(name, suf) {
			name = strings.TrimSuffix(name, suf)
		}
	}

	domain := queue[strings.LastIndex(queue, ".")+1:]

	if strings.EqualFold(domain, name) {
		return ""
	}

	name = strcase.ToDelimited(name, '/')
	if !strings.HasSuffix(name, "/") {
		return name + "/"
	}

	return name

}

func getFieldOptionCustom(f *descriptor.FieldDescriptorProto) string {
	if opts := f.GetOptions(); opts != nil {
		if e, err := proto.GetExtension(opts, goff.E_Custom); err == nil {
			return *(e.(*string))
		}
	}
	return ""
}

func getEnumOptionValue(f *descriptor.EnumValueDescriptorProto) string {
	if opts := f.GetOptions(); opts != nil {
		if e, err := proto.GetExtension(opts, goff.E_Value); err == nil {
			return *(e.(*string))
		}
	}
	return ""
}

func isNullable(f *descriptor.FieldDescriptorProto) bool {
	if opts := f.GetOptions(); opts != nil {
		if e, err := proto.GetExtension(opts, goff.E_Nullable); err == nil {
			return *(e.(*bool))
		}
	}
	return false
}

func uppercaseFirst(str string) string {
	for i, v := range str {
		return string(unicode.ToUpper(v)) + str[i+1:]
	}
	return ""
}

func lowercaseFirst(str string) string {
	for i, v := range str {
		return string(unicode.ToLower(v)) + str[i+1:]
	}
	return ""
}

func getMethodSecured(f *descriptor.MethodDescriptorProto) string {
	if opts := f.GetOptions(); opts != nil {
		if e, err := proto.GetExtension(opts, goff.E_Secured); err == nil {
			return *(e.(*string))
		}
	}
	return ""
}

func goPackageName(fd *descriptor.FileDescriptorProto) string {
	p, _ := g.GetGoPackageName(fd)
	return p
}

func getBsonName(field *descriptor.FieldDescriptorProto, messageName string, modelName string) string {
	bsonName := lowercaseFirst(*field.Name)
	if bsonName == "id" && messageName == modelName {
		return "_id"
	} else {
		return bsonName
	}
}

func getImports(f *descriptor.FileDescriptorProto) map[string]string {
	return g.GetImports(f)
}

func getNatsSubject(f *descriptor.FileDescriptorProto) string {
	if opts := f.GetOptions(); opts != nil {
		if e, err := proto.GetExtension(opts, goff.E_NatsSubject); err == nil {
			return *(e.(*string))
		}
	}
	return ""
}

func goTypeSWithoutStar(typName string) string {
	typ := goTypeS(typName, false)
	typ = strings.TrimPrefix(typ, "*")
	return typ
}

func isEvent(typName string) bool {
	typName = strings.TrimSpace(typName)
	lastIndex := strings.LastIndex(typName, ".")
	if lastIndex >= 0 {
		typName = typName[lastIndex+1:]
	}
	return strings.HasPrefix(strings.ToLower(typName), "event")
}

func debug(field interface{}) string {
	fmt.Println(reflect.TypeOf(field).Name(), field)
	return ""
}

func getEventPackageName(f *descriptor.FileDescriptorProto) string {
	return strcase.ToCamel(g.GetProtoPackage(f))
}

func isBffOrAppTemplate(d *descriptor.FileDescriptorProto) bool {
	name := strings.ToLower(*d.Name)
	if strings.Contains(name, "bff") {
		return true
	}
	if strings.Contains(name, "app") {
		return true
	}
	return false
}

func getEnumValue(f *descriptor.EnumValueDescriptorProto) string {
	enumOption := getEnumOptionValue(f)
	if enumOption != "" {
		return enumOption
	}
	return *f.Name
}

func getServiceNameWithoutEndingSlash(queue string, s *descriptor.ServiceDescriptorProto) string {
	name := getServiceName(queue, s)
	name = strings.TrimSuffix(name, "/")
	return name
}

func getModelCollectionName(m *descriptor.DescriptorProto) string {
	if opts := m.GetOptions(); opts != nil {
		if e, err := proto.GetExtension(opts, goff.E_CollectionName); err == nil {
			return *(e.(*string))
		}
	}
	return ""
}

func getModelDbName(d *descriptor.FileDescriptorProto) string {
	dbName, _ := path2.ExtractDbNameFromRepoPath(*d.Name)
	return fmt.Sprintf("%s%s%s", os.Getenv("DB_PREFIX"), dbName, os.Getenv("DB_SUFFIX"))
}

func getModelIsTenant(m *descriptor.DescriptorProto) bool {
	if opts := m.GetOptions(); opts != nil {
		if e, err := proto.GetExtension(opts, goff.E_IsTenant); err == nil {
			return *(e.(*bool))
		}
	}
	return false
}

func isPointerStrictMode(f *descriptor.FileDescriptorProto) bool {
	isStrictMode := false
	if opts := f.GetOptions(); opts != nil {
		if e, err := proto.GetExtension(opts, goff.E_PointerStrictMode); err == nil {
			isStrictMode = *(e.(*bool))
		}
	}
	return isStrictMode
}

var funcMap = template.FuncMap{
	"GoPackageName":       goPackageName,
	"IsPointerStrictMode": isPointerStrictMode,
	"BackTick": func() string {
		return "`"
	},
	"UppercaseFirst": uppercaseFirst,
	"TsPropertyName": tsPropertyName,
	"LowercaseFirst": lowercaseFirst,
	"Debug":          debug,
	"IsRepeated":     isRepeated,
	"GoType":         goType,
	"TsType":         tsType,
	"JavaType":       javaType,

	"GetImports": getImports,
	// nats subject
	"GetNatsSubject": getNatsSubject,

	"GetJsonName": getJsonName,

	"GetBsonName": getBsonName,

	"GetFieldCustomOption": getFieldCustomOption,

	"GetMethodSecured": getGoMethodSecured,

	"GoTypeS":            goTypeS,
	"JavaTypeS":          javaTypeString,
	"GoTypeSWithoutStar": goTypeSWithoutStar,

	"TsTypeS": tsTypeS,

	"IsEvent": isEvent,

	"GetEventName": getEventName,

	"GetEventPackageName": getEventPackageName,

	"CreateEventNatsNotifierName": createEventNatsNotifierName,

	"CreateEventSocketNotifierName": createEventSocketNotifierName,

	"GetTsImports": getTsImports,

	"IsBffOrAppTemplate": isBffOrAppTemplate,

	"GetEventListenerName": getEventListenerName,
	"IsPointer": func(s string) bool {
		return strings.HasPrefix(s, "*")
	},
	"ToCamel": func(s string) string {
		return strcase.ToCamel(s)
	},
	"DotToSlash": func(s string) string {
		return strings.ReplaceAll(s, ".", "/")
	},
	"GetJavaMethodSecured": getJavaMethodSecured,
	"GetEnumValue":         getEnumValue,

	"GetServiceName": getServiceName,

	"GetServiceNameWithoutEndingSlash": getServiceNameWithoutEndingSlash,

	"GetGoModelMessage": getGoModelMessage,

	"GetGoModelFieldName": getGoModelFieldName,

	"GetGoModelFieldCustomOption": getGoModelFieldCustomOption,

	"DoubleOpenBracket": func() string {
		return "{{"
	},

	"DoubleCloseBracket": func() string {
		return "}}"
	},

	"GetGoMessageExtendFromType": getGoMessageExtendFromType,

	"GetTsMessageExtendFromType": getTsMessageExtendFromType,

	"GetTsSocketPath": getTsSocketPath,

	"GetGoEnumFromType": getGoEnumFromType,

	"GetModelCollectionName": getModelCollectionName,

	"GetModelIsTenant": getModelIsTenant,
	"GetModelDbName":   getModelDbName,
}

var request *plugin.CodeGeneratorRequest
var g *generator.Generator
var useProtoPackageForTsFile = false

func init() {
	viper.SetDefault("proto_package_as_ts_file", false)
}

//go:embed ts.k6.tmpl
var tsK6File string

//go:embed ts.legacy.tmpl
var tsLegacyFile string

//go:embed ts.mobile.tmpl
var tsMobileFile string

//go:embed ts.tmpl
var tsFile string

//go:embed java.tmpl
var javaFile string

//go:embed go.tmpl
var goFile string

func main() {
	viper.AutomaticEnv()
	log.SetPrefix("protoc-gen-goff: ")
	useProtoPackageForTsFile = viper.GetBool("proto_package_as_ts_file")

	// Begin by allocating a generator. The request and response structures are stored there
	// so we can do error handling easily - the response structure contains the field to
	// report failure.
	g = generator.New()

	data, err := ioutil.ReadAll(os.Stdin)
	//data, err := ioutil.ReadFile("/Users/hungnguyen/project/silenteer-oss/goff/out/todo.bff.data")
	if err != nil {
		log.Fatalf("error: reading input: %v", err)
	}

	var response plugin.CodeGeneratorResponse
	if err := proto.Unmarshal(data, g.Request); err != nil {
		log.Fatalf("error: parsing input proto: %v", err)
	}

	request = g.Request

	if len(request.GetFileToGenerate()) == 0 {
		log.Fatal("error: no files to generate")
	}

	g.CommandLineParameters(g.Request.GetParameter())

	// Create a wrapped version of the Descriptors and EnumDescriptors that
	// point to the file that defines them.
	g.WrapTypes()

	g.SetPackageNames()
	g.BuildTypeNameMap()

	g.GenerateAllFiles()

	//goData, _ := ioutil.ReadFile("/Users/hung/project/medi/goff/protoc-gen-goff/go.tmpl")
	//goTmpl, err := template.New(".").Funcs(funcMap).Parse(string(goData))
	goTmpl, err := template.New(".").Funcs(funcMap).Parse(goFile)
	if err != nil {
		log.Fatal(err)
	}

	goModelTmpl, err := template.New(".").Funcs(funcMap).Parse(goModelFile)
	if err != nil {
		log.Fatal(err)
	}

	//tsData, _ := ioutil.ReadFile("/Users/hung/project/medi/goff/protoc-gen-goff/ts.tmpl")
	//tsTmpl, err := template.New(".").Funcs(funcMap).Parse(string(tsData))
	tsTmpl, err := template.New(".").Funcs(funcMap).Parse(tsFile)
	if err != nil {
		log.Fatal(err)
	}

	//tsLegacyData, _ := ioutil.ReadFile("/Users/hung/project/medi/goff/protoc-gen-goff/ts.legacy.tmpl")
	//tsLegacyTmpl, err := template.New(".").Funcs(funcMap).Parse(string(tsLegacyData))
	tsLegacyTmpl, err := template.New(".").Funcs(funcMap).Parse(tsLegacyFile)
	if err != nil {
		log.Fatal(err)
	}

	//tsMobileData, _ := ioutil.ReadFile("/Users/hung/project/medi/goff/protoc-gen-goff/ts.mobile.tmpl")
	//tsMobileTmpl, err := template.New(".").Funcs(funcMap).Parse(string(tsMobileData))
	tsMobileTmpl, err := template.New(".").Funcs(funcMap).Parse(tsMobileFile)
	if err != nil {
		log.Fatal(err)
	}

	tsk6Tmpl, err := template.New(".").Funcs(funcMap).Parse(tsK6File)
	if err != nil {
		log.Fatal(err)
	}

	javaTmpl, err := template.New(".").Funcs(funcMap).Parse(javaFile)
	if err != nil {
		log.Fatal(err)
	}

	for _, name := range request.GetFileToGenerate() {
		var fd *descriptor.FileDescriptorProto

		for _, fd = range request.GetProtoFile() {
			if name == fd.GetName() {
				break
			}
		}
		if fd == nil {
			log.Fatalf("could not find the .proto file for %s", name)
		}

		goModelMessage := getGoModelMessage(fd)
		if goModelMessage != nil {
			var goModelBuf bytes.Buffer
			if err := goModelTmpl.Execute(&goModelBuf, fd); err != nil {
				log.Fatal(err)
			}
			if formattedGoModelBuf, err := format.Source(goModelBuf.Bytes()); err != nil {
				log.Fatalf("error: failed to format generated Go model code %v", err)
			} else {
				goModelBuf = *bytes.NewBuffer(formattedGoModelBuf)
			}

			response.File = append(response.File, &plugin.CodeGeneratorResponse_File{
				Name:    proto.String(goFileName(fd)),
				Content: proto.String(goModelBuf.String()),
			})

			// Only generate model and repo code. Skip other service code generators.
			break // for loop
		}

		//1. begin to generate  Go code
		var goBuf bytes.Buffer
		if err := goTmpl.Execute(&goBuf, fd); err != nil {
			log.Fatal(err)
		}
		if formattedGoBuf, err := format.Source(goBuf.Bytes()); err != nil {
			log.Fatalf("error: failed to format generated Go code %v", err)
		} else {
			goBuf = *bytes.NewBuffer(formattedGoBuf)
		}
		response.File = append(response.File, &plugin.CodeGeneratorResponse_File{
			Name:    proto.String(goFileName(fd)),
			Content: proto.String(goBuf.String()),
		})

		//2. begin to generate Typescript code
		var tsBuf bytes.Buffer
		if err := tsTmpl.Execute(&tsBuf, fd); err != nil {
			log.Fatal(err)
		}
		response.File = append(response.File, &plugin.CodeGeneratorResponse_File{
			Name:    proto.String(tsFileName(fd)),
			Content: proto.String(tsBuf.String()),
		})

		//3. begin to generate Typescript Legacy code
		var tsLegacyBuf bytes.Buffer
		if err := tsLegacyTmpl.Execute(&tsLegacyBuf, fd); err != nil {
			log.Fatal(err)
		}
		response.File = append(response.File, &plugin.CodeGeneratorResponse_File{
			Name:    proto.String(tsLegacyFileName(fd)),
			Content: proto.String(tsLegacyBuf.String()),
		})

		//4. begin to generate java code
		var javaBuf bytes.Buffer
		if err := javaTmpl.Execute(&javaBuf, fd); err != nil {
			log.Fatal(err)
		}
		response.File = append(response.File, &plugin.CodeGeneratorResponse_File{
			Name:    proto.String(javaFileName(fd)),
			Content: proto.String(javaBuf.String()),
		})

		//5. begin to generate Typescript Legacy code
		var tsk6Buf bytes.Buffer
		if err := tsk6Tmpl.Execute(&tsk6Buf, fd); err != nil {
			log.Fatal(err)
		}
		response.File = append(response.File, &plugin.CodeGeneratorResponse_File{
			Name:    proto.String(tsLoadTestFileName(fd)),
			Content: proto.String(tsk6Buf.String()),
		})

		//6. begin to generate Typescript Mobile code
		var tsMobileBuf bytes.Buffer
		if err := tsMobileTmpl.Execute(&tsMobileBuf, fd); err != nil {
			log.Fatal(err)
		}
		response.File = append(response.File, &plugin.CodeGeneratorResponse_File{
			Name:    proto.String(tsMobileFileName(fd)),
			Content: proto.String(tsMobileBuf.String()),
		})
	}

	if data, err = proto.Marshal(&response); err != nil {
		log.Fatalf("error: failed to marshal output proto: %v", err)
	}
	if _, err := os.Stdout.Write(data); err != nil {
		log.Fatalf("error: failed to write output proto: %v", err)
	}
}
