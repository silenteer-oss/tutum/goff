package path

import (
	"strings"
)

//return main database name and sub db name
func ExtractDbNameFromRepoPath(path string) (string, string) {
	dbName := ""
	subName := ""

	if strings.Contains(path, "repo/") {
		pathFromRepo := path[strings.Index(path, "repo/")+5:]
		parts := strings.Split(pathFromRepo, "/")

		if len(parts) > 1 {
			dbName = parts[0]
		}

		if len(parts) > 2 {
			subName = parts[1]
		}
	}
	return dbName, subName
}
