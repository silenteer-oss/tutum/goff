package main

import (
	"bytes"
	"flag"
	"fmt"
	"go/format"
	"io/ioutil"
	"log"
	"os"
	"strings"
	"text/template"
	"unicode"

	"emperror.dev/errors"

	"gitlab.com/silenteer-oss/goff/openapi-gen/codegen"

	"github.com/iancoleman/strcase"

	"github.com/getkin/kin-openapi/openapi3"
)

type Operation struct {
	Operation *openapi3.Operation
	Method    string
}

func uppercaseFirst(str string) string {
	for i, v := range str {
		return string(unicode.ToUpper(v)) + str[i+1:]
	}
	return ""
}

func lowercaseFirst(str string) string {
	for i, v := range str {
		return string(unicode.ToLower(v)) + str[i+1:]
	}
	return ""
}

func getOperation(path *openapi3.PathItem) *Operation {
	if path.Get != nil {
		return &Operation{path.Get, "Get"}
	}

	if path.Post != nil {
		return &Operation{path.Post, "Post"}
	}

	if path.Put != nil {
		return &Operation{path.Put, "Put"}
	}

	if path.Trace != nil {
		return &Operation{path.Trace, "Trace"}
	}

	if path.Connect != nil {
		return &Operation{path.Connect, "Connect"}
	}

	if path.Delete != nil {
		return &Operation{path.Delete, "Delete"}
	}
	if path.Head != nil {
		return &Operation{path.Head, "Head"}
	}
	if path.Options != nil {
		return &Operation{path.Options, "Options"}
	}

	if path.Patch != nil {
		return &Operation{path.Patch, "Patch"}
	}

	log.Fatal(fmt.Sprintf("Open api path=%s", path.Ref))
	return &Operation{nil, ""}
}

func toCamel(value string) string {
	return strcase.ToCamel(value)
}

var funcMap = template.FuncMap{
	"UppercaseFirst": uppercaseFirst,
	"LowercaseFirst": lowercaseFirst,
	"GetOperation":   getOperation,
	"ToCamel":        toCamel,
	"GetFuncParams": func(path openapi3.PathItem) map[string]string {
		result := map[string]string{}
		opt := getOperation(&path)
		for _, param := range opt.Operation.Parameters {
			if param.Value != nil && param.Value.Schema != nil {
				schema, _ := codegen.GenerateGoSchema(param.Value.Schema, []string{""})
				result[param.Value.Name] = schema.GoType
			}
		}
		if opt.Operation.RequestBody != nil && opt.Operation.RequestBody.Value != nil {
			if s, ok := opt.Operation.RequestBody.Value.Content["application/json"]; ok {
				schema, _ := codegen.GenerateGoSchema(s.Schema, []string{""})
				result["body"] = schema.GoType
			}
		}

		return result
	},

	"GetResponse": func(path openapi3.PathItem) string {
		opt := getOperation(&path)
		if res, ok := opt.Operation.Responses["default"]; ok {
			if c, ok := res.Value.Content["application/json"]; ok {
				schema, _ := codegen.GenerateGoSchema(c.Schema, []string{""})
				goType := schema.GoType
				if strings.Contains(goType, "AdditionalProperties map[string]interface{}") {
					return "map[string]interface{}"
				}
				return schema.GoType
			}
		}
		return ""
	},

	"GenerateTypesForSchemas": func(schemas map[string]*openapi3.SchemaRef) []codegen.TypeDefinition {
		definitions, err := generateTypesForSchemas(schemas)
		if err != nil {
			log.Fatal(err)
		}

		return definitions
	},

	"HasBody": func(path openapi3.PathItem) bool {
		opt := getOperation(&path)
		if opt.Operation.RequestBody != nil && opt.Operation.RequestBody.Value != nil {
			if _, ok := opt.Operation.RequestBody.Value.Content["application/json"]; ok {
				return true
			}
		}
		return false
	},
	"ToSnake": func(str string) string {
		return strcase.ToSnake(str)
	},
}

type Data struct {
	PackageName string
	Swagger     *openapi3.Swagger
}

func main() {
	log.SetPrefix("openapi-gen: ")
	var (
		outputFile      string
		openApiSpecFile string
		packageName     string
	)
	flag.StringVar(&packageName, "p", "client", "Please specify Go package name")
	flag.StringVar(&outputFile, "o", "", "Please specify where to output Go file")
	flag.Parse()

	if flag.NArg() < 1 {
		log.Fatal("Missing  open api 3.0 specification file")
		os.Exit(1)
	}

	openApiSpecFile = flag.Arg(0)

	swagger, err := openapi3.NewSwaggerLoader().LoadSwaggerFromFile(openApiSpecFile)
	if err != nil {
		log.Fatal("error loading swagger spec\n: %s", err)
	}

	goTmpl, err := template.New(".").Funcs(funcMap).Parse(goFile)
	if err != nil {
		log.Fatal(err)
	}
	//1. begin to generate  Go code
	data := Data{
		PackageName: packageName,
		Swagger:     swagger,
	}

	var goBuf bytes.Buffer
	if err := goTmpl.Execute(&goBuf, data); err != nil {
		log.Fatal(err)
	}
	if formattedGoBuf, err := format.Source(goBuf.Bytes()); err != nil {
		log.Fatalf("error: failed to format generated Go code %v", err)
	} else {
		goBuf = *bytes.NewBuffer(formattedGoBuf)
	}
	ioutil.WriteFile(outputFile, goBuf.Bytes(), 0777)
	log.Println("Done.")

}

// Generates type definitions for any custom types defined in the
// components/schemas section of the Swagger spec.
func generateTypesForSchemas(schemas map[string]*openapi3.SchemaRef) ([]codegen.TypeDefinition, error) {
	types := make([]codegen.TypeDefinition, 0)
	// We're going to define Go types for every object under components/schemas
	for _, schemaName := range codegen.SortedSchemaKeys(schemas) {
		schemaRef := schemas[schemaName]

		goSchema, err := codegen.GenerateGoSchema(schemaRef, []string{schemaName})
		if err != nil {
			return nil, errors.Wrap(err, fmt.Sprintf("error converting Schema %s to Go type", schemaName))
		}

		types = append(types, codegen.TypeDefinition{
			JsonName: schemaName,
			TypeName: codegen.SchemaNameToTypeName(schemaName),
			Schema:   goSchema,
		})

		types = append(types, goSchema.GetAdditionalTypeDefs()...)
	}

	return types, nil
}
