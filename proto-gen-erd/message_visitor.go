package main

import (
	"sort"
	"strconv"

	"emperror.dev/errors"
	"github.com/emicklei/proto"
)

type MessageVisitor struct {
	message  *Message
	Messages []Message
}

type Message struct {
	IsModel        bool
	IsTenant       bool
	ModelName      string
	CollectionName string //mongodb table name
	Fields         []Field
	Err            error
}

type Field struct {
	Name     string
	Type     string
	Repeated bool
	Ref      string
}

func (v *MessageVisitor) VisitMessage(m *proto.Message) {
	v.message = &Message{
		ModelName: m.Name,
	}

	fieldVisitor := NewFieldVisitor()
	for i := range m.Elements {
		m.Elements[i].Accept(fieldVisitor)
	}

	v.message.Fields = append(v.message.Fields, fieldVisitor.Fields...)

	v.Messages = append(v.Messages, *v.message)

	sort.Sort(ByCollectionName(v.Messages))
}

func (v *MessageVisitor) VisitOption(o *proto.Option) {
	if v.message == nil {
		return
	}
	if o.Name == "(goff.model)" {
		isModel, err := strconv.ParseBool(o.Constant.Source)
		if err != nil {
			v.message.Err = errors.WithMessage(err, "parsing IsModel error ")
		} else {
			v.message.IsModel = isModel
		}
	}

	if o.Name == "(goff.collection_name)" {
		v.message.CollectionName = o.Constant.Source
	}

	if o.Name == "(goff.is_tenant)" {
		isTenant, err := strconv.ParseBool(o.Constant.Source)
		if err != nil {
			v.message.Err = errors.WithMessage(err, "parsing IsTenant error ")
		} else {
			v.message.IsTenant = isTenant
		}
	}
}

func (v *MessageVisitor) VisitService(sv *proto.Service)        {}
func (v *MessageVisitor) VisitSyntax(s *proto.Syntax)           {}
func (v *MessageVisitor) VisitPackage(p *proto.Package)         {}
func (v *MessageVisitor) VisitImport(i *proto.Import)           {}
func (v *MessageVisitor) VisitNormalField(i *proto.NormalField) {}
func (v *MessageVisitor) VisitEnumField(i *proto.EnumField)     {}
func (v *MessageVisitor) VisitEnum(e *proto.Enum)               {}
func (v *MessageVisitor) VisitComment(e *proto.Comment)         {}
func (v *MessageVisitor) VisitOneof(o *proto.Oneof)             {}
func (v *MessageVisitor) VisitOneofField(o *proto.OneOfField)   {}
func (v *MessageVisitor) VisitReserved(r *proto.Reserved)       {}
func (v *MessageVisitor) VisitRPC(r *proto.RPC)                 {}
func (v *MessageVisitor) VisitMapField(f *proto.MapField)       {}

// proto2
func (v *MessageVisitor) VisitGroup(g *proto.Group)           {}
func (v *MessageVisitor) VisitExtensions(e *proto.Extensions) {}

// implement sort interface
type ByCollectionName []Message

func (a ByCollectionName) Len() int {
	return len(a)
}

func (a ByCollectionName) Less(i, j int) bool {
	return true
}

func (a ByCollectionName) Swap(i, j int) {
	a[i], a[j] = a[j], a[i]
}
