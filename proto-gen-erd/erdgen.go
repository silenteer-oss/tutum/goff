package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
	"text/template"
	"unicode"

	path_util "gitlab.com/silenteer-oss/goff/protoc-gen-goff/pkg/path"

	"github.com/iancoleman/strcase"

	"emperror.dev/errors"

	"github.com/emicklei/proto"
)

func uppercaseFirst(str string) string {
	for i, v := range str {
		return string(unicode.ToUpper(v)) + str[i+1:]
	}
	return ""
}

func lowercaseFirst(str string) string {
	for i, v := range str {
		return string(unicode.ToLower(v)) + str[i+1:]
	}
	return ""
}

func toCamel(value string) string {
	return strcase.ToCamel(value)
}

var funcMap = template.FuncMap{
	"UppercaseFirst": uppercaseFirst,
	"LowercaseFirst": lowercaseFirst,
	"ToCamel":        toCamel,
	"ToSnake": func(str string) string {
		return strcase.ToSnake(str)
	},

	"ToLowerCamel": func(str string) string {
		return strcase.ToLowerCamel(str)
	},

	"GetRefModelName": func(str string) string {
		if strings.Index(str, ".") > 0 {
			return strings.Split(str, ".")[0]
		}

		if strings.Index(str, ":") > 0 {
			return strings.Split(str, ":")[0]
		}
		return ""
	},

	"GetRefId": func(str string) string {
		if strings.Index(str, ".") > 0 {
			return lowercaseFirst(strings.Split(str, ".")[1])
		}

		if strings.Index(str, ":") > 0 {
			return lowercaseFirst(strings.Split(str, ":")[1])
		}
		return ""
	},
	"GetFieldType": func(str string) string {
		if str == "goff.UUID" {
			return "uuid"
		}
		return str
	},

	"GetFieldName": func(field Field) string {
		if field.Repeated {
			return "[]" + lowercaseFirst(field.Name)
		}
		return lowercaseFirst(field.Name)
	},
}

type DbConfig struct {
	Data       map[string]map[string][]Message
	ConfigPkg  string
	FixturePkg string
}

//return db name -> sub db name -> Model
func ParseProtoFiles(repoDir string) (map[string]map[string][]Message, error) {
	databases := map[string]map[string][]Message{}

	err := filepath.Walk(repoDir,
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return errors.WithMessage(err, fmt.Sprintf("loading file error  '%s'", path))
			}

			if info.IsDir() || !strings.HasSuffix(path, ".proto") {
				return nil
			}

			if path == "" || path == "." || path == ".." {
				return nil
			}

			reader, _ := os.Open(path)
			defer reader.Close()

			parser := proto.NewParser(reader)
			definition, err := parser.Parse()
			if err != nil {
				return errors.WithMessage(err, fmt.Sprintf("parse file error  '%s'", path))
			}

			dbName, subName := path_util.ExtractDbNameFromRepoPath(path)

			visitor := &MessageVisitor{
				Messages: []Message{},
			}

			proto.Walk(definition,
				proto.WithService(func(service *proto.Service) {

				}),
				proto.WithOption(func(option *proto.Option) {
					option.Accept(visitor)
				}),
				proto.WithMessage(func(m *proto.Message) {
					m.Accept(visitor)
				}),
			)

			//messages := visitor.Messages

			//for i := 0; i < len(messages); i++ {
			//	fmt.Println(messages[i])
			//}

			db, ok := databases[dbName]
			if !ok {
				db = map[string][]Message{}
			}

			subDb, ok := db[subName]
			if !ok {
				subDb = []Message{}
			}
			subDb = append(subDb, visitor.Messages...)
			db[subName] = subDb

			databases[dbName] = db

			return nil
		})

	if err != nil {
		return nil, err
	}

	return databases, nil
}

func GenerateCode(templateFile string, config DbConfig, outputFile string) {
	goTmpl, err := template.New(".").Funcs(funcMap).Parse(templateFile)
	if err != nil {
		log.Fatal(err)
	}

	var goBuf bytes.Buffer
	if err := goTmpl.Execute(&goBuf, config); err != nil {
		log.Fatal(err)
	}

	if outputFile != "" {
		err := ioutil.WriteFile(outputFile, goBuf.Bytes(), 0777)
		if err != nil {
			log.Fatal(err)
		}
	} else {
		fmt.Println(goBuf.String())
	}
}
