package main

import (
	"github.com/emicklei/proto"
)

type FieldVisitor struct {
	Fields []Field
}

func (f *FieldVisitor) VisitMessage(m *proto.Message) {
}

func (f *FieldVisitor) VisitService(v *proto.Service) {
}

func (f *FieldVisitor) VisitSyntax(s *proto.Syntax) {
}

func (f *FieldVisitor) VisitPackage(p *proto.Package) {
}

func (f *FieldVisitor) VisitOption(o *proto.Option) {
}

func (f *FieldVisitor) VisitImport(i *proto.Import) {
}

func (f *FieldVisitor) VisitNormalField(i *proto.NormalField) {
	field := Field{
		Name:     i.Name,
		Type:     i.Type,
		Repeated: i.Repeated,
	}

	for _, option := range i.Options {
		if option.Name == "(goff.ref)" {
			field.Ref = option.Constant.Source
		}
	}
	f.Fields = append(f.Fields, field)
}

func (f *FieldVisitor) VisitEnumField(i *proto.EnumField) {
}

func (f *FieldVisitor) VisitEnum(e *proto.Enum) {
}

func (f *FieldVisitor) VisitComment(e *proto.Comment) {
}

func (f *FieldVisitor) VisitOneof(o *proto.Oneof) {
}

func (f *FieldVisitor) VisitOneofField(o *proto.OneOfField) {
}

func (f *FieldVisitor) VisitReserved(r *proto.Reserved) {
}

func (f *FieldVisitor) VisitRPC(r *proto.RPC) {
}

func (f *FieldVisitor) VisitMapField(m *proto.MapField) {
}

func (f *FieldVisitor) VisitGroup(g *proto.Group) {
}

func (f *FieldVisitor) VisitExtensions(e *proto.Extensions) {
}

func NewFieldVisitor() *FieldVisitor {
	return &FieldVisitor{
		Fields: []Field{},
	}
}
