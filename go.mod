module gitlab.com/silenteer-oss/goff

go 1.16

require (
	emperror.dev/errors v0.7.0
	github.com/emicklei/proto v1.9.0
	github.com/fatih/structtag v1.2.0
	github.com/getkin/kin-openapi v0.8.0
	github.com/go-playground/validator/v10 v10.4.1 // indirect
	github.com/golang/protobuf v1.5.2
	github.com/google/uuid v1.2.0 // indirect
	github.com/iancoleman/strcase v0.2.0
	github.com/pkg/errors v0.9.1
	github.com/spf13/viper v1.10.1
	github.com/stretchr/objx v0.2.0 // indirect
	gitlab.com/silenteer-oss/titan v1.0.69
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519 // indirect
	golang.org/x/net v0.0.0-20211015210444-4f30a5c0130f // indirect
	golang.org/x/sys v0.0.0-20220405210540-1e041c57c461 // indirect
	golang.org/x/time v0.0.0-20201208040808-7e3f01d25324 // indirect
	google.golang.org/protobuf v1.28.0
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
)
